﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameController : MonoBehaviour {
    public GameObject StoryCanvas;
    // Use this for initialization
    void Awake()
    {
        Time.timeScale = 0f;
        StoryCanvas.SetActive(true);
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.anyKeyDown)
        {
            Time.timeScale = 1f;
            StoryCanvas.SetActive(false);
        }
	}
}
