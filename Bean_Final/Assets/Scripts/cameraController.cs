﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraController : MonoBehaviour {
    public GameObject player;
    public float cameraSpeed = 5f;
    Vector3 offset;
    // Use this for initialization
    void Start () {
        offset = transform.position - player.transform.position;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    void LateUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        transform.Rotate(0f, moveHorizontal * Time.deltaTime * cameraSpeed, 0f);
        if (player != null)
        {
            transform.position = player.transform.position + offset;
        }
    }
}
