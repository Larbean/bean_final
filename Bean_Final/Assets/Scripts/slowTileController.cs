﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class slowTileController : MonoBehaviour {

    public float setto = 5f;
    void Awake()
    {
        
    }
    void OnCollisionEnter(Collision collision)
    {
        
        if (collision.gameObject.name == "Player")
        {
            
            playerController PlayerController = collision.gameObject.GetComponent<playerController>();
            
            //print("On");

            PlayerController.speed = setto;
            
        }
        

    }
    
    void OnCollisionExit(Collision collision)
    {
        
        if (collision.gameObject.name == "Player")
        {
            //print("Off");
            playerController PlayerController = collision.gameObject.GetComponent<playerController>();
            
            PlayerController.speed = PlayerController.originalSpeed;
        }
    }
    
}
