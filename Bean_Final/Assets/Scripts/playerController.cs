﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class playerController : MonoBehaviour {
    Rigidbody rb;
    public float speed;
    public float jumpSpeed = 10f;
    public float distanceToGround = 0.5f;
    public float originalSpeed;
    public Transform camPivot;
    public float originalJumpSpeed;
    public Text timeScore;
    float timeAdd;
    void Start () {
        originalSpeed = speed;
        originalJumpSpeed = jumpSpeed;
        rb = GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update () {
        timeAdd += Time.deltaTime;
        timeScore.text = "Time: " + timeAdd.ToString();
        if(rb.position.y < 500)
        {
            Death();
        }

    }
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Space) && IsGrounded() && rb.velocity.y < .5f)
        {
            //print("Jump");
            //rb.AddForce(0, 11f, 0);
            Vector3 jumpVelocity = new Vector3(0f, jumpSpeed, 0f);
            rb.velocity = rb.velocity + jumpVelocity;
        }
        //float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector3 movement = camPivot.forward * moveVertical * speed;//new Vector3(0f, 0f, moveVertical * speed);
        rb.AddForce(movement /** speed*/);
    }
    bool IsGrounded()
    {
        return Physics.Raycast(transform.position, Vector3.down, distanceToGround);
    }
    void Death()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    void OnTriggerEnter(Collider other)
    {
        
        if (other.tag == "NextLevel")
        {
            SceneManager.LoadScene(1);

        }
        if (other.tag == "Won")
        {
            SceneManager.LoadScene(0);
            //win conditions
        }
    }
}
