﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class highScoreTimer : MonoBehaviour {
    public static int time;
    int highscore;

    public Text text;

    void Awake()
    {
        highscore = PlayerPrefs.GetInt("Best Time: ", 0);

        time = 0;
    }
    void Update()
    {

        text.text = "Best Time: " + time.ToString();
        if(time > highscore)
        {
            highscore = time;
            PlayerPrefs.SetInt("Best Time: ", highscore);
        }
    }
}