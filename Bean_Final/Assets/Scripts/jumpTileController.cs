﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jumpTileController : MonoBehaviour {
    
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Player")
        {
            playerController PlayerController = collision.gameObject.GetComponent<playerController>();
            PlayerController.jumpSpeed = 15;
            //print("JUMP SPEED");

        }
   
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.name == "Player")
        {
            playerController PlayerController = collision.gameObject.GetComponent<playerController>();
            PlayerController.jumpSpeed = PlayerController.originalJumpSpeed;
            //print("NORMAL JUMPSPEED");
        }
        
    }
}
